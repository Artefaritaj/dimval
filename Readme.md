# DimVal

A library to manage dimensioned values with more than f64 precision.
The lowest value is 10^-24.

Useful to parse physical user input:

```
assert_eq!((DimVal::from_str("1mm").unwrap() + DimVal::from_str("3 cm").unwrap()).to_string(), "31mm");
```