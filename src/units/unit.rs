// @Author: Lashermes Ronan <ronan>
// @Date:   14-04-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 14-04-2017
// @License: MIT

use std::collections::{HashMap};
use std::str::FromStr;
use std::fmt;

use std::sync::Mutex;

use units::{Dimension};

use failure::Error;

lazy_static! {
    pub static ref UNITS_SYMBOLS: Mutex<HashMap<String, Unit>> = {//<Symbol, Unit>
        let mut m = HashMap::new();
        m.insert(format!(""), Unit::new_prim_dimstr("", "Unit", "").unwrap());

        //primary
        m.insert(format!("mol"), Unit::new_prim_dimstr("mol", "mole", "mol").unwrap());
        m.insert(format!("m"), Unit::new_prim_dimstr("m", "meter", "m").unwrap());
        m.insert(format!("kg"), Unit::new_prim_dimstr("kg", "kilogram", "kg").unwrap());
        m.insert(format!("s"), Unit::new_prim_dimstr("s", "second", "s").unwrap());
        m.insert(format!("A"), Unit::new_prim_dimstr("A", "ampere", "A").unwrap());
        m.insert(format!("K"), Unit::new_prim_dimstr("K", "kelvin", "K").unwrap());
        m.insert(format!("cd"), Unit::new_prim_dimstr("cd", "candela", "cd").unwrap());

        //derived
        m.insert(format!("V"), Unit::new_prim_dimstr("kg.m2.s-3.A-1", "volt", "V").unwrap());
        m.insert(format!("Hz"), Unit::new_prim_dimstr("s-1", "hertz", "Hz").unwrap());
        m.insert(format!("N"), Unit::new_prim_dimstr("kg.m.s-2", "newton", "N").unwrap());
        m.insert(format!("Pa"), Unit::new_prim_dimstr("kg.m-1.s-2", "pascal", "Pa").unwrap());
        m.insert(format!("J"), Unit::new_prim_dimstr("kg.m2.s-2", "joule", "J").unwrap());
        m.insert(format!("W"), Unit::new_prim_dimstr("kg.m2.s-3", "watt", "W").unwrap());
        m.insert(format!("Ω"), Unit::new_prim_dimstr("kg.m2.s-3.A-2", "ohm", "Ω").unwrap());
        m.insert(format!("ohm"), Unit::new_prim_dimstr("kg.m2.s-3.A-2", "ohm", "Ω").unwrap());
        Mutex::new(m)
    };
}

#[derive(Debug,Clone,PartialEq,Eq)]
pub struct Unit {
    dimension: Dimension,
    name: String,
    symbol: String
}

impl Unit {

    pub fn add_unit(symbol: String, unit: Unit) -> Result<(), Error> {
        UNITS_SYMBOLS.lock().map_err(|_| format_err!("Cannot get hold of UNITS_SYMBOLS"))?.insert(symbol, unit);
        Ok(())
    }

    pub fn get_name(&self) -> &str {
        &self.name
    }

    pub fn get_symbol(&self) -> &str {
        &self.symbol
    }

    pub fn get_dimension(&self) -> &Dimension {
        &self.dimension
    }

    pub fn to_dimension(self) -> Dimension {
        self.dimension
    }

    pub fn new(dim: Dimension, name: &str, symbol: &str) -> Unit {
        Unit { dimension: dim, name: name.to_string(), symbol: symbol.to_string() }
    }

    pub fn new_prim_dimstr(dim: &str, name: &str, symbol: &str) -> Result<Unit, Error> {
        Ok(Unit { dimension: try!(Dimension::parse_prim_dim(dim)), name: name.to_string(), symbol: symbol.to_string() })
    }

    pub fn new_dimstr(dim: &str, name: &str, symbol: &str) -> Result<Unit, Error> {
        Ok(Unit { dimension: try!(Dimension::parse_dim(dim)), name: name.to_string(), symbol: symbol.to_string() })
    }



}

impl FromStr for Unit {
    type Err = Error;
    fn from_str(s: &str) -> Result<Unit, Error> {

        let guard = UNITS_SYMBOLS.lock().map_err(|_|format_err!("Cannot get hold of UNITS_SYMBOLS"))?;
        for (sym, u) in guard.iter() {
            if sym == s {
                return Ok(u.clone());
            }
        }
        Err(format_err!("{} is not a known unit", s))
    }
}

impl fmt::Display for Unit {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.get_symbol())
    }
}



#[test]
fn test_unit() {
    assert!(Unit::from_str("S").is_err());
    assert!(Unit::add_unit(format!("S"), Unit::new_dimstr("ohm-1", "Siemens", "S").unwrap()).is_ok());
    assert!(Unit::from_str("N").is_ok());
    assert!(Unit::from_str("S").is_ok());

    let siemens = Unit::from_str("S").unwrap();
    let _ = Unit::new(siemens.to_dimension(), "Siemens", "S");
}
