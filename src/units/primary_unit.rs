// @Author: Lashermes Ronan <ronan>
// @Date:   14-04-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 14-04-2017
// @License: MIT

use std::collections::HashMap;
use std::str::FromStr;
use std::fmt;

use failure::Error;

#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash,Serialize,Deserialize)]
pub enum PrimaryUnit {
    Length,
    Mass,
    Time,
    ElectricCurrent,
    Temperature,
    AmountOfSubstance,
    LuminousIntensity
}

lazy_static! {
    pub static ref PRIMARY_SYMBOLS: HashMap<String, PrimaryUnit> = {
        let mut m = HashMap::new();
        m.insert(format!("mol"), PrimaryUnit::AmountOfSubstance);
        m.insert(format!("m"), PrimaryUnit::Length);
        m.insert(format!("kg"), PrimaryUnit::Mass);
        m.insert(format!("s"), PrimaryUnit::Time);
        m.insert(format!("A"), PrimaryUnit::ElectricCurrent);
        m.insert(format!("K"), PrimaryUnit::Temperature);
        m.insert(format!("cd"), PrimaryUnit::LuminousIntensity);
        m
    };
}

impl FromStr for PrimaryUnit {
    type Err = Error;
    fn from_str(s: &str) -> Result<PrimaryUnit, Error> {
        for (sym, pu) in PRIMARY_SYMBOLS.iter() {
            if sym == s {
                return Ok(*pu);
            }
        }
        Err(format_err!("{} is not a known primary unit", s))
    }
}

impl fmt::Display for PrimaryUnit {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = match self {
            &PrimaryUnit::Length                 => "m",
            &PrimaryUnit::Mass                   => "kg",
            &PrimaryUnit::Time                   => "s",
            &PrimaryUnit::ElectricCurrent        => "A",
            &PrimaryUnit::Temperature            => "K",
            &PrimaryUnit::AmountOfSubstance      => "mol",
            &PrimaryUnit::LuminousIntensity      => "cd",
        };

        write!(f, "{}", s)
    }
}

#[test]
pub fn test_pu() {
    assert!(PrimaryUnit::from_str("pi").is_err());
    for (s, p) in PRIMARY_SYMBOLS.iter() {
        assert!(PrimaryUnit::from_str(s).is_ok());
        let u = PrimaryUnit::from_str(s).unwrap();
        assert_eq!(u,*p);
        assert_eq!(&u.to_string(), s);
    }
}
