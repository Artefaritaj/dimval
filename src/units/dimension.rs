

use std::collections::{HashMap, BTreeMap};
use std::str::FromStr;
use std::ops::{Mul, Div};

use failure::Error;

use units::{PrimaryUnit, Unit, PRIMARY_SYMBOLS, UNITS_SYMBOLS};

#[derive(Debug,Clone,PartialEq,Eq,Serialize,Deserialize)]
pub struct Dimension {
    dimension: HashMap<PrimaryUnit, i64>
}

impl Dimension {
    pub fn new() -> Dimension {
        Dimension { dimension: HashMap::new() }
    }

    pub fn from_hashmap(map: HashMap<PrimaryUnit, i64>) -> Dimension {
        Dimension { dimension: map }
    }

    pub fn get_hashmap(&self) -> &HashMap<PrimaryUnit, i64> {
        &self.dimension
    }

    pub fn parse_dim(s: &str) -> Result<Dimension, Error> {
        let mut working = s.to_string();
        let mut result = HashMap::new();

        while working.len() > 0 {
            let (sym, w2) = try!(Dimension::next_unit_symbol(working));
            let (exp_u, w3) = try!(Dimension::next_exponent(w2));
            working = w3;

            for (pu, exp_pu) in sym.get_dimension().get_hashmap().iter() {
                let prev_exp = match result.get(pu) {
                    Some(i) => *i,
                    None => 0
                };
                result.insert(*pu, prev_exp + exp_pu*exp_u);
            }
        }

        Ok(Dimension::from_hashmap(result))
    }

    pub fn parse_prim_dim(s: &str) -> Result<Dimension, Error> {
        let mut working = s.to_string();
        let mut result = HashMap::new();

        while working.len() > 0 {
            let (sym, w2) = try!(Dimension::next_primary_symbol(working));
            let (exp, w3) = try!(Dimension::next_exponent(w2));
            working = w3;

            let prev_exp = match result.get(&sym) {
                Some(i) => *i,
                None => 0
            };

            result.insert(sym, prev_exp + exp);
        }

        Ok(Dimension::from_hashmap(result))
    }

    fn next_unit_symbol(mut s: String) -> Result<(Unit, String), Error> {
        s = s.trim().trim_left_matches('.').to_string();
        let mut ranking: BTreeMap<usize, (Unit, usize)> = BTreeMap::new();
        //priority to longer symbol

        let guard = UNITS_SYMBOLS.lock().map_err(|_|format_err!("Cannot get hold of UNITS_SYMBOLS"))?;

        for (sym, u) in guard.iter() {
            if s.starts_with(sym) {
                ranking.insert(sym.chars().count(), (u.clone(), sym.len()));
            }
        }

        let sym_opt = ranking.iter().next_back();
        match sym_opt {
            Some((_, &(ref u, ref len))) => {
                let _ = s.drain(..*len);
                Ok((u.clone(), s))
            },
            None => {
                Err(format_err!("No symbol found in {}", s))
            }
        }
    }

    fn next_primary_symbol(mut s: String) -> Result<(PrimaryUnit, String), Error> {
        s = s.trim().trim_left_matches(".").to_string();
        let mut ranking: BTreeMap<usize, (PrimaryUnit, usize)> = BTreeMap::new();
        //priority to longer symbol

        for (sym, pu) in PRIMARY_SYMBOLS.iter() {
            if s.starts_with(sym) {
                ranking.insert(sym.chars().count(), (*pu, sym.len()));
            }
        }

        let sym_opt = ranking.iter().next_back();
        match sym_opt {
            Some((_, &(ref pu, ref len))) => {
                let _ = s.drain(..*len);
                Ok((*pu, s))
            },
            None => {
                Err(format_err!("No symbol found in {}", s))
            }
        }
    }

    fn next_exponent(mut s: String) -> Result<(i64, String), Error> {
        s = s.trim().to_string();

        let mut char_ind = 0;
        for c in s.chars() {
            if char::is_numeric(c) || ( c == '-' && char_ind == 0) {//'-' is authorized as first character only
                char_ind += 1;
            }
            else {
                break;
            }
        }

        let n_str: String = s.drain(..char_ind).collect();
        let n = if n_str.len() > 0 {
            try!(n_str.parse::<i64>().map_err(|e|format_err!("{}", e)))
        }
        else {
            1
        };
        Ok((n,s))
    }
}

impl FromStr for Dimension {
    type Err = Error;
    fn from_str(s: &str) -> Result<Dimension, Error> {
        Dimension::parse_dim(s)
    }
}

impl From<PrimaryUnit> for Dimension {
    fn from(pu: PrimaryUnit) -> Dimension {
        let mut hm = HashMap::new();
        hm.insert(pu, 1);
        Dimension { dimension: hm }
    }
}

impl<'a> Mul<&'a Dimension> for Dimension {
    type Output = Dimension;
    fn mul(self, other: &'a Dimension) -> Dimension {
        let mut result: HashMap<PrimaryUnit, i64> = HashMap::new();
        let mut other_hm = other.get_hashmap().clone();

        for (pd, exp) in self.dimension {
            if let Some(other_dim) = other_hm.remove(&pd) {
                result.insert(pd, exp + other_dim);
            }
            else {
                result.insert(pd, exp);
            }
        }

        for (pd, exp) in other_hm {
            result.insert(pd, exp);
        }

        Dimension::from_hashmap(result)
    }
}

impl<'a> Div<&'a Dimension> for Dimension {
    type Output = Dimension;
    fn div(self, other: &'a Dimension) -> Dimension {
        let mut result: HashMap<PrimaryUnit, i64> = HashMap::new();
        let mut other_hm = other.get_hashmap().clone();

        for (pd, exp) in self.dimension {
            if let Some(other_dim) = other_hm.remove(&pd) {
                result.insert(pd, exp - other_dim);
            }
            else {
                result.insert(pd, exp);
            }
        }

        for (pd, exp) in other_hm {
            result.insert(pd, -exp);
        }

        Dimension::from_hashmap(result)
    }
}

#[test]
fn test_parse_dim() {
    let area = Dimension::parse_prim_dim("m2").unwrap();
    assert!(*area.dimension.get(&PrimaryUnit::Length).unwrap() == 2);

    let volt = Dimension::parse_prim_dim("kg.m2.s-3.A-1").unwrap();
    println!("Volt is {:?}", volt);

    let _ = Dimension::parse_dim("N.m").unwrap();
    // println!("Joule is {:?}", joule);
}
