/*
 * Author:   ronan.lashermes@inria.fr
 * Creation: Thu Nov 09 2017
 *
 * Copyright (c) 2017 INRIA
 */

extern crate bigdecimal;
extern crate num_traits;
#[macro_use] extern crate lazy_static;
extern crate num;
extern crate regex;
extern crate serde;
#[macro_use] extern crate serde_derive;
// #[macro_use] extern crate failure_derive;
#[macro_use] extern crate failure;

pub mod units;
pub mod dimval;
pub mod dimval_builder;
pub mod prefixes;
pub mod errors;

pub use self::dimval_builder::DimValBuilder;
pub use self::dimval::DimVal;
pub use self::prefixes::*;
pub use self::units::*;
