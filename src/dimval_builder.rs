/*
 * Created on Mon Feb 26 2018
 *
 * Copyright (c) 2018 INRIA
 */

use dimval::DimVal;
use prefix::*;
use units::*;

use std::str::FromStr;

#[derive(Debug)]
pub struct DimValBuilder {
    val_res: Result<DimVal, String>
}

impl DimValBuilder {
    pub fn new() -> DimValBuilder {
        DimValBuilder { val_res: Ok(DimVal::new()) }
    }

    pub fn build(self) -> Result<DimVal, String> {
        self.val_res
    }

    pub fn value(self, val_str: &str) -> DimValBuilder {
        if let Ok(mut val) = self.val_res {
            if let Ok(bigi) = DimVal::parse_number_str(val_str, 0) {
                val.set_baseval(bigi);
                DimValBuilder { val_res: Ok(val) }
            }
            else {
                DimValBuilder { val_res: Err(format!("Cannot parse value: {}", val_str)) }
            }
        }
        else {
            DimValBuilder { val_res: self.val_res }
        }
    }

    pub fn prefix(self, prefix_str: &str) -> DimValBuilder {
        if let Ok(val) = self.val_res {
            if let Ok(prefix) = Prefix::from_str(prefix_str) {
                let pow10_res = val.pow10(prefix.to_exponent() as i64).map_err(|e|e.to_string());
                DimValBuilder { val_res: pow10_res }
            }
            else {
                DimValBuilder { val_res: Err(format!("Cannot parse {} as prefix.", prefix_str))}
            }
        }
        else {
            DimValBuilder { val_res: self.val_res }
        }
    }

    pub fn dimension(self, dim_str: &str) -> DimValBuilder {
        if let Ok(mut val) = self.val_res {
            if let Ok(dim) = Dimension::from_str(dim_str) {
                val.set_dimension(dim);
                DimValBuilder { val_res: Ok(val) }
            }
            else {
                DimValBuilder { val_res: Err(format!("Cannot parse {} as prefix.", dim_str))}
            }
        }
        else {
            DimValBuilder { val_res: self.val_res }
        }
    }
}

#[test]
fn test_builder() {
    let truth = DimVal::from_str("-3.5nm").unwrap();

    let built = DimValBuilder::new().value("-3.5").prefix("n").dimension("m").build().unwrap();

    assert_eq!(truth, built);
}