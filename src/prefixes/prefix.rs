// @Author: Lashermes Ronan <ronan>
// @Date:   14-04-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 14-04-2017
// @License: MIT

use std::collections::HashMap;
use std::str::FromStr;
use std::fmt;

use failure::Error;

pub const BASE_VAL_EXP: i32 = -24;

lazy_static! {
    pub static ref PREFIX_SYMBOLS: HashMap<String, Prefix> = {
        let mut m = HashMap::new();
        m.insert(format!("Y"), Prefix::Yotta);
        m.insert(format!("Z"), Prefix::Zetta);
        m.insert(format!("E"), Prefix::Exa);
        m.insert(format!("P"), Prefix::Peta);
        m.insert(format!("T"), Prefix::Tera);
        m.insert(format!("G"), Prefix::Giga);
        m.insert(format!("M"), Prefix::Mega);
        m.insert(format!("k"), Prefix::Kilo);
        m.insert(format!("h"), Prefix::Hecto);
        m.insert(format!("da"), Prefix::Deca);
        m.insert(format!(""), Prefix::None);
        m.insert(format!("d"), Prefix::Deci);
        m.insert(format!("c"), Prefix::Centi);
        m.insert(format!("m"), Prefix::Milli);
        m.insert(format!("μ"), Prefix::Micro);
        m.insert(format!("u"), Prefix::Micro);
        m.insert(format!("n"), Prefix::Nano);
        m.insert(format!("p"), Prefix::Pico);
        m.insert(format!("f"), Prefix::Femto);
        m.insert(format!("a"), Prefix::Atto);
        m.insert(format!("z"), Prefix::Zepto);
        m.insert(format!("y"), Prefix::Yocto);
        m
    };
}

#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub enum Prefix {
    Yotta,
    Zetta,
    Exa,
    Peta,
    Tera,
    Giga,
    Mega,
    Kilo,
    Hecto,
    Deca,
    None,
    Deci,
    Centi,
    Milli,
    Micro,
    Nano,
    Pico,
    Femto,
    Atto,
    Zepto,
    Yocto
}

impl Prefix {
    pub fn select_from_exponent(exp: f64) -> Result<Prefix, Error> {
        match exp {
            x if x >= 24.0 => Ok(Prefix::Yotta),
            x if x >= 21.0 => Ok(Prefix::Zetta),
            x if x >= 18.0 => Ok(Prefix::Exa),
            x if x >= 15.0 => Ok(Prefix::Peta),
            x if x >= 12.0 => Ok(Prefix::Tera),
            x if x >= 9.0 => Ok(Prefix::Giga),
            x if x >= 6.0 => Ok(Prefix::Mega),
            x if x >= 3.0 => Ok(Prefix::Kilo),
            x if x >= 0.0 => Ok(Prefix::None),
            x if x >= -3.0 => Ok(Prefix::Milli),
            x if x >= -6.0 => Ok(Prefix::Micro),
            x if x >= -9.0 => Ok(Prefix::Nano),
            x if x >= -12.0 => Ok(Prefix::Pico),
            x if x >= -15.0 => Ok(Prefix::Femto),
            x if x >= -18.0 => Ok(Prefix::Atto),
            x if x >= -21.0 => Ok(Prefix::Zepto),
            x if x >= -24.0 => Ok(Prefix::Yocto),
            _ => Err(format_err!("No prefix for exponent {}", exp))
        }
    }

    pub fn to_exponent(&self) -> i32 {
        match self {
            &Prefix::Yotta      => 24,
            &Prefix::Zetta      => 21,
            &Prefix::Exa        => 18,
            &Prefix::Peta       => 15,
            &Prefix::Tera       => 12,
            &Prefix::Giga       => 9,
            &Prefix::Mega       => 6,
            &Prefix::Kilo       => 3,
            &Prefix::Hecto      => 2,
            &Prefix::Deca       => 1,
            &Prefix::None       => 0,
            &Prefix::Deci       => -1,
            &Prefix::Centi      => -2,
            &Prefix::Milli      => -3,
            &Prefix::Micro      => -6,
            &Prefix::Nano       => -9,
            &Prefix::Pico       => -12,
            &Prefix::Femto      => -15,
            &Prefix::Atto       => -18,
            &Prefix::Zepto      => -21,
            &Prefix::Yocto      => -24,
        }
    }

    pub fn to_symbol(&self) -> &str {
        match self {
            &Prefix::Yotta      => "Y",
            &Prefix::Zetta      => "Z",
            &Prefix::Exa        => "E",
            &Prefix::Peta       => "P",
            &Prefix::Tera       => "T",
            &Prefix::Giga       => "G",
            &Prefix::Mega       => "M",
            &Prefix::Kilo       => "k",
            &Prefix::Hecto      => "h",
            &Prefix::Deca       => "da",
            &Prefix::None       => "",
            &Prefix::Deci       => "d",
            &Prefix::Centi      => "c",
            &Prefix::Milli      => "m",
            &Prefix::Micro      => "μ",
            &Prefix::Nano       => "n",
            &Prefix::Pico       => "p",
            &Prefix::Femto      => "f",
            &Prefix::Atto       => "a",
            &Prefix::Zepto      => "z",
            &Prefix::Yocto      => "y",
        }
    }
}

impl FromStr for Prefix {
    type Err = Error;
    fn from_str(s: &str) -> Result<Prefix, Error> {
        for (sym, p) in PREFIX_SYMBOLS.iter() {
            if sym == s {
                return Ok(p.clone());
            }
        }
        Err(format_err!("{} is not a known unit", s))
    }
}

impl From<Prefix> for i32 {
    fn from(p: Prefix) -> i32 {
        p.to_exponent()
    }
}

impl fmt::Display for Prefix {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.to_symbol())
    }
}

#[test]
fn test_prefixes() {
    let atto = "a";
    let pre = atto.parse::<Prefix>().unwrap();
    assert!(pre.to_exponent() == -18);


    for (s,p) in PREFIX_SYMBOLS.iter() {
        if s == "u" {//edge case for usability
            continue;
        }

        assert_eq!(p.to_symbol(), s);
        let exp = p.to_exponent();
        let newp = Prefix::select_from_exponent(exp as f64).unwrap();
        if exp%3 == 0 {//remove correct edge cases (do not parse -2, -1, 1, 2 for usability)
            assert_eq!(&newp, p, "For exponent {}: Source = {}, Parsed = {}", exp, p, newp);
        }
    }
}
