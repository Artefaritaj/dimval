// @Author: Lashermes Ronan <ronan>
// @Date:   14-04-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 14-04-2017
// @License: MIT

use num::bigint::BigInt;
use bigdecimal::BigDecimal;
use num::{ToPrimitive};
use num::{Zero, One, pow};

use std::collections::{BTreeMap};
use std::cmp::Ordering;
use std::str::FromStr;
use std::fmt;
use std::ops::{Add, Neg, Sub, Mul, Div, Rem, AddAssign};

use units::{UNITS_SYMBOLS, Unit, Dimension};
use prefixes::{Prefix, BASE_VAL_EXP};

use regex::Regex;

use failure::Error;

lazy_static! {
    static ref DIMVAL_REGEX: Regex = Regex::new(r"^\s*(-?\d+[.]?\d*(?:[eE]-?\d+)?)\s*([[:alpha:]\p{Greek}]*)[ ]*$").unwrap();
}

#[derive(Clone,PartialEq,Eq,Serialize,Deserialize)]
pub struct DimVal {
    base_val: BigInt,//number of increments of the atomic unit
    dimension: Dimension
}

impl PartialOrd for DimVal {
    fn partial_cmp(&self, other: &DimVal) -> Option<Ordering> {
        Some(self.base_val.cmp(&other.base_val))
    }
}

impl Ord for DimVal {
    fn cmp(&self, other: &DimVal) -> Ordering {
        self.base_val.cmp(&other.base_val)
    }
}

impl Add for DimVal {
    type Output = DimVal;
    fn add(self, other: DimVal) -> DimVal {
        DimVal { base_val: self.base_val + other.base_val, dimension: self.dimension }
    }
}

impl<'a> AddAssign<&'a DimVal> for DimVal {
    fn add_assign(&mut self, other: &'a DimVal) {
        self.base_val += &other.base_val;
    }
}

impl<'a> Add<&'a DimVal> for DimVal {
    type Output = DimVal;
    fn add(self, other: &'a DimVal) -> DimVal {
        DimVal { base_val: self.base_val + &other.base_val, dimension: self.dimension }
    }
}

impl<'a> Mul<&'a DimVal> for DimVal {
    type Output = DimVal;
    /// CAN PANIC !!!!!!!!!!!!!!!!!!!!!!
    fn mul(self, other: &'a DimVal) -> DimVal {
        let toobig = DimVal { base_val: self.base_val * &other.base_val, dimension: self.dimension * &other.dimension };
        toobig.pow10(BASE_VAL_EXP as i64).unwrap()
    }
}

impl Mul for DimVal {
    type Output = DimVal;
    /// CAN PANIC !!!!!!!!!!!!!!!!!!!!!!
    fn mul(self, other: DimVal) -> DimVal {
        let toobig = DimVal { base_val: self.base_val * other.base_val, dimension: self.dimension * &other.dimension };
        toobig.pow10(BASE_VAL_EXP as i64).unwrap()
    }
}

impl Mul<DimVal> for i64 {
    type Output = DimVal;
    fn mul(self, other: DimVal) -> DimVal {
        DimVal { base_val: self * other.base_val, dimension: other.dimension }
    }
}

impl Div<DimVal> for DimVal {
    type Output = DimVal;
    /// CAN PANIC !!!!!!!!!!!!!!!!!!!!!!!!
    fn div(self, other: DimVal) -> DimVal {
        let toolittle = DimVal { base_val: self.base_val / other.base_val, dimension: self.dimension / &other.dimension };
        toolittle.pow10(-BASE_VAL_EXP as i64).unwrap()
    }
}

impl<'a> Div<&'a DimVal> for DimVal {
    type Output = DimVal;
    /// CAN PANIC !!!!!!!!!!!!!!!!!!!!!!!!
    fn div(self, other: &'a DimVal) -> DimVal {
        let toolittle = DimVal { base_val: self.base_val / &other.base_val, dimension: self.dimension / &other.dimension };
        toolittle.pow10(-BASE_VAL_EXP as i64).unwrap()
    }
}

impl Rem for DimVal {
    type Output = DimVal;
    fn rem(self, other: DimVal) -> DimVal {
        DimVal { base_val: self.base_val % other.base_val, dimension: self.dimension }
    }
}


impl<'a> Rem<&'a DimVal> for DimVal {
    type Output = DimVal;
    fn rem(self, other: &'a DimVal) -> DimVal {
        DimVal { base_val: self.base_val % &other.base_val, dimension: self.dimension }
    }
}

impl Div<i64> for DimVal {
    type Output = DimVal;
    /// CAN PANIC !!!!!!!!!!!!!!!!!!!!!!!!
    fn div(self, other: i64) -> DimVal {
        DimVal { base_val: self.base_val / other, dimension: self.dimension }
    }
}

impl Sub for DimVal {
    type Output = DimVal;
    fn sub(self, other: DimVal) -> DimVal {
        DimVal { base_val: self.base_val - other.base_val, dimension: self.dimension }
    }
}

impl<'a> Sub<&'a DimVal> for DimVal {
    type Output = DimVal;
    fn sub(self, other: &'a DimVal) -> DimVal {
        DimVal { base_val: self.base_val - &other.base_val, dimension: self.dimension }
    }
}

impl Zero for DimVal {
    fn zero() -> DimVal {
        DimVal { base_val: BigInt::zero(), dimension: Dimension::new() }
    }

    fn is_zero(&self) -> bool {
        self.base_val.is_zero()
    }
}

impl One for DimVal {
    fn one() -> DimVal {
        DimVal { base_val: BigInt::one(), dimension: Dimension::new() }
    }
}

impl Neg for DimVal {
    type Output = DimVal;
    fn neg(mut self) -> DimVal {
        self.base_val = -self.base_val;
        self
    }
}

impl DimVal {
    fn from_part(val: BigInt, dim: Dimension) -> DimVal {
        DimVal { base_val: val, dimension: dim }
    }

    pub fn new() -> DimVal {
        DimVal { base_val: BigInt::zero(), dimension: Dimension::new() }
    }


    // fn from_f64_parts(val: f64, prefix: Prefix, dimension: Dimension) -> Result<DimVal, String> {
    //     DimVal::from_f64_parts_raw(val, prefix.to_exponent(), dimension)
    // }

    pub fn from_f64_parts_raw(val: f64, exp: i32, dimension: Dimension) -> Result<DimVal, Error> {
        let prefixed_val = DimVal::parse_number_f64(val, exp)?;
        Ok(DimVal { base_val: prefixed_val, dimension: dimension })
    }

    // pub fn from_parts_str(val: f64, prefix: &str, dimension: &str) -> Result<DimVal, String> {
    //     let p2 = try!(Prefix::from_str(prefix));
    //     let d2 = try!(Dimension::from_str(dimension));

    //     DimVal::from_parts(val, p2, d2)
    // }

    //num must be filled
    fn regex_num_text(s: &str) -> Result<(String, String), Error> {
        match DIMVAL_REGEX.captures_iter(s).next() {
            Some(c) => {
                if c.len() == 3 {
                    let num_str = c[1].to_string();
                    let text_str = c[2].to_string();
                    Ok((num_str, text_str))
                }
                else {
                    Err(format_err!("Regex error on {}", s))
                }
            },
            None => { Err(format_err!("No value can be read in {}", s)) }
        }
    }

    pub fn get_dimension(&self) -> &Dimension {
        &self.dimension
    }

    pub fn set_dimension(&mut self, new_dim: Dimension) {
        self.dimension = new_dim
    }

    pub(crate) fn set_baseval(&mut self, new_baseval: BigInt) {
        self.base_val = new_baseval;
    }

    pub fn to_f64_prefix<I: Into<i32>>(&self, e: I) -> Result<f64, Error> {
        let bigd = BigDecimal::from((self.base_val.clone(), (-BASE_VAL_EXP + e.into()) as i64));
        bigd.to_f64().ok_or(format_err!("Cannot convert to f64"))
    }

    pub fn to_f64(&self) -> Result<f64, Error> {
        self.to_f64_prefix(0)
    }

    pub fn to_i64(&self) -> Result<i64, Error> {
        self.to_i64_prefix(0)
    }

    pub fn to_i64_prefix<I: Into<i32>>(&self, e: I) -> Result<i64, Error> {
        let bigd = BigDecimal::from((self.base_val.clone(), (-BASE_VAL_EXP + e.into()) as i64));
        bigd.to_i64().ok_or(format_err!("Cannot convert to f64"))
    }

    pub fn filter_unit_str(self, u: &str) -> Result<DimVal, Error> {
        let guard = UNITS_SYMBOLS.lock().map_err(|_|format_err!("Cannot get hold of UNITS_SYMBOLS"))?;
        let u = guard.get(u).ok_or(format_err!("Cannot find unit {}", u))?;

        if &self.dimension == u.get_dimension() {
            Ok(self)
        }
        else {
            Err(format_err!("{} has not the correct {} unit", self.to_string(), u))
        }
    }

    pub fn filter_unit(self, u: &Unit) -> Result<DimVal, Error> {
        if &self.dimension == u.get_dimension() {
            Ok(self)
        }
        else {
            Err(format_err!("{} has not the correct {} unit", self.to_string(), u))
        }
    }

    pub fn is_unit(&self, u: &Unit) -> bool {
        &self.dimension == u.get_dimension()
    }

    pub fn pow10(&self, exp10: i64) -> Result<DimVal, Error> {
        let bigd = BigDecimal::from((self.base_val.clone(), -exp10));
        let inflated_str = DimVal::sanitize_bigd_str(&bigd.to_string())?;
        let new_big = BigInt::from_str(&inflated_str)?;
        Ok(DimVal { base_val: new_big, dimension: self.dimension.clone() })
    }

    // pub fn from_f64(f: f64) -> Result<DimVal, String> {
    //     DimVal::from_f64_parts(f, Prefix::None, Dimension::new())
    // }

    pub fn get_unit_string(&self) -> Result<String, Error> {
        let mut unit_str = String::new();
        let guard = UNITS_SYMBOLS.lock().map_err(|_|format_err!("Cannot get hold of UNITS_SYMBOLS"))?;
        for (_, u) in guard.iter() {
            if u.get_dimension() == &self.dimension {
                unit_str = u.to_string();
                break;
            }
        }

        if unit_str == "" {
            for (pu, pu_exp) in self.dimension.get_hashmap().iter() {
                unit_str += &pu.to_string();
                if *pu_exp != 1 {
                    unit_str += &pu_exp.to_string();
                }
            }
        }
        Ok(unit_str)
    }

    /// Convert a big int into a big float representation
    fn to_number_str(val: &BigInt, prefix: Prefix) -> String {
        let bigd = BigDecimal::from((val.clone(), (-BASE_VAL_EXP + prefix.to_exponent()) as i64));

        // bigdecimal.to_string() is buggy -> sanitize output
        let  bigd_str = bigd.to_string();

        let mut splitted: Vec<&str> = bigd_str.split(".").collect();

        while splitted.len() < 2 {
            splitted.push("");
        }

        let mut unit_str = splitted[0].trim_left_matches("0");

        if unit_str.is_empty() {
            unit_str = "0";
        }

        let decimal_str = splitted[1].trim_right_matches("0");

        if decimal_str.is_empty() {
            format!("{}", unit_str)
        }
        else {
            format!("{}.{}", unit_str, decimal_str)
        }
    }

    fn parse_number_f64<I: Into<i32>>(number_f64: f64, prefix: I) -> Result<BigInt, Error> {
        let bigd = BigDecimal::from(number_f64);
        let inflated = bigd * pow(BigDecimal::from(10), (-BASE_VAL_EXP + prefix.into()) as usize);
        let inflated_str = DimVal::sanitize_bigd_str(&inflated.to_string())?;
        Ok(BigInt::from_str(&inflated_str)?)
    }

    pub(crate) fn parse_number_str<I: Into<i32>>(number_str: &str, prefix: I) -> Result<BigInt, Error> {
        let bigd = BigDecimal::from_str(number_str)?;

        let inflated = bigd * pow(BigDecimal::from(10), (-BASE_VAL_EXP + prefix.into()) as usize);
        let inflated_str = DimVal::sanitize_bigd_str(&inflated.to_string())?;

        Ok(BigInt::from_str(&inflated_str)?)
    }

    fn sanitize_bigd_str(bigd_str: &str) -> Result<String, Error> {
        //split in unit_str "." decimal_str
        let split_dot: Vec<&str> = bigd_str.split(".").collect();

        if split_dot.len() < 1 {
            return Err(format_err!("Nothing to sanitize"));
        }

        Ok(split_dot[0].to_string())
    }

    pub(crate) fn parse_exponent_unit_str(exp_unit_str: &str) -> Result<(Prefix, Dimension), Error> {
        let mut text_str = exp_unit_str.to_string();

        //unit has priority...
        let mut ranking: BTreeMap<usize, (Unit, usize)> = BTreeMap::new();
        let guard = UNITS_SYMBOLS.lock().map_err(|_|format_err!("Cannot get hold of UNITS_SYMBOLS"))?;
        for (sym, u) in guard.iter() {
            if text_str.ends_with(sym) {
                ranking.insert(sym.chars().count(), (u.clone(), sym.len()));
            }
        }

        //longest unit win
        let sym_opt = ranking.iter().next_back();
        let dimension = match sym_opt {
            Some((_, &(ref u, ref len))) => {

                let end_ind = text_str.len();
                let _ = text_str.drain(end_ind-len..);
                u.get_dimension().clone()
            },
            None => Dimension::new()
        };

        //...then prefix
        let prefix_str = text_str.trim();
        let prefix = try!(prefix_str.parse::<Prefix>());

        Ok((prefix, dimension))
    }
}

impl FromStr for DimVal {
    type Err = Error;
    ///Only one unit can be used: '1 Vs' is invalid. '1mm' is 1 millimeter.
    fn from_str(s: &str) -> Result<DimVal, Error> {
        let (num_str, text_str) = try!(DimVal::regex_num_text(s));

        //println!("num {}, text {}", num_str, text_str);
        //read val
        // let val = try!(num_str.parse::<f64>().map_err(|e|e.to_string()));
        let (prefix, dimension) = DimVal::parse_exponent_unit_str(&text_str)?;
        let val = DimVal::parse_number_str(&num_str, prefix)?;


        // DimVal::from_f64_parts(val, prefix, dimension)
        Ok(DimVal::from_part(val, dimension))
    }
}

impl fmt::Debug for DimVal {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "DimVal {{ {} }}", self.to_string())
    }
}

impl fmt::Display for DimVal {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {

        //compute prefix using f64 approximation
        let fval_abs = try!(self.base_val.to_f64().ok_or(fmt::Error::default())).abs();
        let exp = if fval_abs > 0f64 {
            fval_abs.log10()
        }
        else {
            -BASE_VAL_EXP as f64
        };

        let pre = try!(Prefix::select_from_exponent(exp+(BASE_VAL_EXP as f64)).map_err(|_|fmt::Error::default()));
        // println!("prefix is {}", pre);

        //compute number string from value and prefix
        let number_str = DimVal::to_number_str(&self.base_val, pre);
        // let final_f = try!(self.to_f64_prefix(pre).map_err(|_|fmt::Error::default()));
        // println!("final_f is {}", final_f);
        let prefix_str = pre.to_symbol();

        let mut unit_str = String::new();
        let guard = UNITS_SYMBOLS.lock().map_err(|_|fmt::Error::default())?;
        for (_, u) in guard.iter() {
            if u.get_dimension() == &self.dimension {
                unit_str = u.to_string();
                break;
            }
        }

        if unit_str == "" {
            for (pu, pu_exp) in self.dimension.get_hashmap().iter() {
                unit_str += &pu.to_string();
                if *pu_exp != 1 {
                    unit_str += &pu_exp.to_string();
                }
            }
        }

        write!(f, "{}{}{}", number_str, prefix_str, unit_str)
    }
}


#[cfg(test)]
mod tests {
    use dimval::*;
    use DimValBuilder;
    // use units::*;
    use prefix::*;
    use std::str::FromStr;

    #[test]
    fn test_regex() {
        let valid_tests = vec!["  -23.55 μV", " 2e-3 Ω", " 3.14e5TV", "1", "2.d", "2.e4", "14", "-20mm"];

        for test in valid_tests {
            assert!(DIMVAL_REGEX.is_match(test), "{} should have matched", test);

            // for c in DIMVAL_REGEX.captures_iter(test) {
            //     println!("Capture: {:?}", c);
            // }
        }

        let invalid_tests = vec!["--3", "4e3.5", "3. 14"];
        for test in invalid_tests {
            assert!(DIMVAL_REGEX.is_match(test) == false, "{} should not match", test);
        }
    }

    #[test]
    fn test_parse_dimval() {
        let tests = vec!["  -23.55 μV", " 2e-4 Ω", " 3.14e5TV", "-20mm", "0mm"];

        for test in tests.iter() {
            let dimval = test.parse::<DimVal>().unwrap();
            println!("DimVal for {} is {:?} => {}", test, dimval, dimval);
        }
    }

    #[test]
    fn test_parse_number() {
        let test_vec = vec!["02.", "3", "3.1400", ".40", "3000.0003", "0", "0.0", "1.0009E-4"];

        for test in test_vec {
            let pre = Prefix::None;
            let _ = DimVal::parse_number_str(test, pre).unwrap();
            // println!("{}", big);
        }
    }

    #[test]
    fn test_to_string() {
        let tests = vec!["  -23.55 μV", " 2e-4 Ω", " 3.14e5TV", "-20mm", "0mm"];
        for test in tests.iter() {
            let dimval = test.parse::<DimVal>().unwrap();
            let tostr = dimval.to_string();
            println!("DimVal for {} is {}", test, tostr);
        }
    }

    // #[test]
    // fn test_to_f64() {
    //     let test = "2e-3 Ω";
    //     let dv = test.parse::<DimVal>().unwrap();
    //     let f = dv.to_f64_prefix(Prefix::Milli).unwrap();
    //     assert!(f == 2.0, "f is {}, should be 2.0", f);
    // }

    #[test]
    fn test_approx() {
        let test = "2.4uV";
        let truth = "2.4μV";
        let dv = test.parse::<DimVal>().unwrap();
        assert_eq!(dv.to_string(), truth, "{} instead of {}", dv.to_string(), truth);
    }

    #[test]
    fn test_doc() {
        assert_eq!((DimVal::from_str("1mm").unwrap() + DimVal::from_str("3 cm").unwrap()).to_string(), "31mm");
    }


    #[test]
    fn to_f64_i64() {
        let dv = DimVal::from_str("3s").unwrap();
        assert!(dv.to_f64().unwrap() - 3f64 < 0.00001f64);
        assert_eq!(dv.to_i64().unwrap(), 3i64);

        assert!(3000f64 - dv.to_f64_prefix(-3).unwrap() < 0.00001f64);
        assert_eq!(dv.to_i64_prefix(-3).unwrap(), 3000i64);
    }

    #[test]
    fn test_pow10() {
        let dv = DimVal::from_str("3.1us").unwrap();
        let dv_truth = DimVal::from_str("31ns").unwrap();

        assert_eq!(dv.pow10(-2).unwrap(), dv_truth);
    }

    #[test]
    fn test_mul() {
        let a = DimVal::from_str("3.1us").unwrap();
        let a2 = DimVal::from_str("3.1us").unwrap();
        let b = DimVal::from_str("-2.5nm").unwrap();

        let c: i64 = 12;

        let ab = DimValBuilder::new().value("-7.75").prefix("f").dimension("ms").build().unwrap();
        // let ab = DimVal::from_str("-7.75fms").unwrap();
        assert_eq!(a*b, ab);
        // println!("a*b={}", a*b);

        let ca = DimVal::from_str("37.2us").unwrap();
        assert_eq!(c*a2, ca);
    }

    #[test]
    fn test_div() {
        let a = DimVal::from_str("3.1us").unwrap();
        let b = DimVal::from_str("-2.5nm").unwrap();

        let c: i64 = 5;

        let ab = DimValBuilder::new().value("-1240").dimension("sm-1").build().unwrap();
        assert_eq!(a/b, ab);
        // println!("a*b={}", a*b);

        let abc = ab / c;
        let truth = DimValBuilder::new().value("-248").dimension("sm-1").build().unwrap();
        assert_eq!(abc, truth);
    }

    #[test]
    fn test_rem() {
        let a = DimVal::from_str("15ns").unwrap();
        let b = DimVal::from_str("4ns").unwrap();


        let ab = DimValBuilder::new().value("3").prefix("n").dimension("s").build().unwrap();
        assert_eq!(a%b, ab);
    }
}